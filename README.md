# Pokemon Test

We would like you to create a small Android app that connects to the Pokémon API 
and displays at least two different resources - pokémon, ability, etc. It can be 
as complicated or as simple as you want, but you should want to impress us.

The app should:
 - be compatible with JellyBean and above
 - incorporate material design in some way
 - display the data in user-friendly way

The pokémon api can be found here: http://www.pokeapi.co 
A Kotlin based wrapper exists for this API, please don’t use it, we would like
to see how you connect to custom API.

This task is to test the structure, style and patterns of your code so the 
design and layout of the user interface is up to you. 

Good luck!

Please clone this project and create a new branch, when you are done we will add 
you add as guest so you can push your example to the repo.